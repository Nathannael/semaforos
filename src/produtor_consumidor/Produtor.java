package produtor_consumidor;

public class Produtor extends Thread {
	Semaforo mutex;
	Semaforo empty;
	Semaforo full;
	int recurso;
	public Produtor(Semaforo mutex, Semaforo empty, Semaforo full, int recurso) {
		super("produtor");
		this.mutex = mutex;
		this.empty = empty;
		this.full = full;
		this.recurso = recurso;
	}
	
	@Override
	public void run()  {
		while (true) {
			try {
				empty.down();
				mutex.down();
				recurso++;
				System.out.println("Produzi um item. Recurso = " + recurso);
				mutex.up();
				full.up();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			try {
				sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}
