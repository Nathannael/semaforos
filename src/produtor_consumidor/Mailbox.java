package produtor_consumidor;

public class Mailbox {
	
	public static void main(String[] args) {
		Semaforo mutex = new Semaforo();
		Semaforo full = new Semaforo(0);
		Semaforo empty = new Semaforo(10);
		int recurso = 0;
		Produtor p = new Produtor(mutex, empty, full, recurso);
		Consumidor c = new Consumidor(mutex, empty, full, recurso);
		p.start();
		c.start();
	}
}
