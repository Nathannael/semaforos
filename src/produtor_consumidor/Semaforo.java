package produtor_consumidor;

public class Semaforo {
	private int valor;
	
	public Semaforo() {
		this(1);
	}

	public Semaforo(int valor) {
		this.valor = valor;
	}
	
	public synchronized void down() throws InterruptedException  {
		while (true) {
			if (valor > 0) {
				valor --;
				break;
			} else {
				wait();
			}
		}
	}
	
	public synchronized void up() throws InterruptedException  {
		valor++;
		notifyAll();
	}
}