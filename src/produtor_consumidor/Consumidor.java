package produtor_consumidor;

public class Consumidor extends Thread {
	Semaforo mutex;
	Semaforo empty;
	Semaforo full;
	int recurso;
	public Consumidor (Semaforo mutex, Semaforo empty, Semaforo full, int recurso) {
		super("consumidor");
		this.mutex = mutex;
		this.empty = empty;
		this.full = full;
		this.recurso = recurso;
	}
	
	@Override
	public void run()  {
		while (true) {
			try {
				full.down();
				mutex.down();
				recurso--;
				System.out.println("consumi um item. Recurso = " + recurso);
				mutex.up();
				empty.up();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			try {
				sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
